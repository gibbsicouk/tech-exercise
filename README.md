# FDP Group Tech Test - Full Stack Developer

## Introduction

You should not spend longer than a few hours developing code and don’t worry if you don’t manage to complete it, just send over what you have. We’d like to see your solution with well thought out, clean code focusing on readability and reusability.

## Brief

The aim is to produce a webpage which displays a list of friends/followers as close as possible to the screenshot. These should be retrieved from the mock API classes provided. The product must be as adaptable as possible for future requirements, for example the implementation of additional API classes. The code should be formatted and organised as simply as possible and repeated code should be avoided.

## Requirement

This tech exercise require PHP 7.4 to work based on the following instruction.

## Instruction

To run the base tech ercercise in a browser open the root directory of the code in the terminal and use the command:

**"php -S localhost:8000 -t . "**

Open the link "localhost:8000" in your browser

### Mock APIs

### CBook API Mock Library
The CBook API uses an approach which requires:

 - A client_id (**`8XWr1Hp5xbhSM6u0`**)
 - A client_secret (**`07xdHM9OA8ULaBeC`**)

### Critter API Mock Library
The Critter API requires just an API key for authentication:

 - An API Key (**`j4nHNuaPo2nq6AdW`**)

### Lgram API Mock Library
The Lgram API uses an approach which requires:

 - A client_id (**`8XWr1Hp5xbhSM6u0`**)
 - A client_secret (**`07xdHM9OA8ULaBeC`**)

### Outputting the Response
The final output of this implementation will be a page showing the data as close as possible as the screenshot provided. The screenshot has taken from a webpage based on Bootstrap 5.1.3.

## Deliverables

 - A brief write-up of any decisions you made (and why!) and details of any bits you didn't manage to add but you'd like to.
 - An archived git repository showing commit history.

All the deliverables should be emailed to luca.s@fdpgroup.co.uk once you have finished.
