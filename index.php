<?php
include('template-parts/head.php');
// Include API files
include('src/api/CBookAPI.php');
include('src/api/CritterAPI.php');
include('src/api/LgramAPI.php');

// Create instane of each API
$cbookapi = new CBookAPI('8XWr1Hp5xbhSM6u0', '07xdHM9OA8ULaBeC');
$critterapi = new CritterAPI('j4nHNuaPo2nq6AdW');
$lgramapi = new LgramAPI('8XWr1Hp5xbhSM6u0', '07xdHM9OA8ULaBeC');

// Access methods to be used in foreach loop
$cbooks = (array) $cbookapi->getFriends();
$critters = (array) $critterapi->getFollowers();
$lgrams = (array) $lgramapi->getBuddies();

?>
<div id="tech-exercise">
    <h1>Associates</h1>

    <div class="e-hr"></div>

    <?php // friends from $cbooks markup 

        /*
        * Step 1: each of the values in the $cbook array have been called as such:
        *         $cbook->friend_name
        *
        * Step 2: once each of the values were rendered and working, the HTML markup 
        *         and styling was written with relevant class names assigned (c-panel, c-entry)
        */
    ?>
    <div class="c-panel">
        <div class="c-panel__title">
            <h2>Friends</h2>
        </div>
        <div class="c-panel__content">
            <? foreach($cbooks["friends"] as $cbook): ?>
                <div class="c-entry flex">
                    <div class="c-entry__img col">
                        <img src="<?= $cbook->friend_image; ?>" alt="friend logo">
                    </div>
                    <div class="c-entry__meta col">
                        <h3><?= $cbook->friend_name; ?></h3>
                        <p><?= $cbook->friend_bio; ?></p>
                        <a href="<?= $cbook->email; ?>"><?= $cbook->email; ?></a>
                    </div>
                    <div class="c-entry__count col">
                        <span class="count flex"><?= $cbook->number_of_friends; ?> <img class="i-user" src="<?= get_template_directory_uri(); ?>/tech-exercise/images/tabler-icon-users.svg" alt="user icon"></span>
                        
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <?php // followers from $critters markup 

        /*
        * Step 3: once the first array and values were working with the markup, a simple
        *         find and replace on $cbook, friends and each of the values from the .json
        *         files for $critters and $lgrams was done
        */
    ?>
    <div class="c-panel">
        <div class="c-panel__title">
            <h2>Followers</h2>
        </div>
        <div class="c-panel__content">
            <? foreach($critters["followers"] as $critter): ?>
                <div class="c-entry flex">
                    <div class="c-entry__img col">
                        <img src="<?= $critter->photo; ?>" alt="follower logo">
                    </div>
                    <div class="c-entry__meta col">
                        <h3><?= $critter->name; ?></h3>
                        <p><?= $critter->description; ?></p>
                        <a href="<?= $critter->email; ?>"><?= $critter->email; ?></a>
                    </div>
                    <div class="c-entry__count col">
                        <span class="count flex"><?= $critter->follower_count; ?> <img class="i-user" src="<?= get_template_directory_uri(); ?>/tech-exercise/images/tabler-icon-users.svg" alt="user icon"></span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <?php // buddies from $lgrams markup ?>
    <div class="c-panel">
        <div class="c-panel__title">
            <h2>Buddies</h2>
        </div>
        <div class="c-panel__content">
            <? foreach($lgrams["buddies"] as $lgram): ?>
                <div class="c-entry flex">
                    <div class="c-entry__img col">
                        <img src="<?= $lgram->buddy_image; ?>" alt="buddy logo">
                    </div>
                    <div class="c-entry__meta col">
                        <h3><?= $lgram->buddy_name; ?></h3>
                        <p><?= $lgram->buddy_bio; ?></p>
                        <a href="<?= $lgram->email; ?>"><?= $lgram->email; ?></a>
                    </div>
                    <div class="c-entry__count col">
                        <span class="count flex"><?= $lgram->buddies_count; ?> <img class="i-user" src="<?= get_template_directory_uri(); ?>/tech-exercise/images/tabler-icon-users.svg" alt="user icon"></span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<?php include('template-parts/footer.php'); ?>